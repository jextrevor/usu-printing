'use strict'

var express = require('express')
const { spawn } = require('child_process')
const { randomUUID } = require('crypto')
const fs = require('fs/promises')
const path = require('path')
const { check, validationResult } = require('express-validator')
const sleep = require('sleep-promise')

var app = express()

app.use(express.json({ limit: '100mb' }))

app.use(express.static(path.join(__dirname, '../client/build')))

const checkQueue = async (printerName) =>
  new Promise((resolve, reject) => {
    const process = spawn('lpstat', ['-o', printerName])
    let hasOutput = false
    process.stdout.on('data', (data) => {
      hasOutput = true
    })
    process.on('close', (code) => {
      if (code !== 0) {
        reject(code)
      } else {
        resolve(!hasOutput)
      }
    })
  })

const deletePrinter = async (printerName) =>
  new Promise((resolve, reject) => {
    const process = spawn('lpadmin', ['-x', printerName])
    process.on('close', (code) => {
      if (code !== 0) {
        reject(code)
      } else {
        resolve()
      }
    })
  })

const createPrinter = async (printerName, aNumber, colored) =>
  new Promise((resolve, reject) => {
    const process = spawn('lpadmin', [
      '-p',
      printerName,
      '-E',
      '-v',
      'lpd://' +
        aNumber +
        '@vmpps4.aggies.usu.edu/Campus-' +
        (colored ? 'Color' : 'BW'),
      '-m',
      'drv:///sample.drv/generic.ppd',
    ])
    process.on('close', (code) => {
      if (code !== 0) {
        reject(code)
      } else {
        resolve()
      }
    })
  })

const startPrint = async (printerName, title, twoSided, landscape, filename) =>
  new Promise((resolve, reject) => {
    const process = spawn('lpr', [
      '-P',
      printerName,
      ...(title ? ['-J', title] : []),
      ...(twoSided === 'long'
        ? ['-o', 'sides=two-sided-long-edge']
        : twoSided === 'short'
        ? ['-o', 'sides=two-sided-short-edge']
        : []),
      ...(landscape ? ['-o', 'landscape'] : []),
      filename,
    ])
    process.on('close', (code) => {
      if (code !== 0) {
        reject(code)
      } else {
        resolve()
      }
    })
  })

const removeFileWithLogging = async (filename, printerName) => {
  try {
    await fs.rm(filename)
    console.log(`Removed the file for ${printerName}`)
  } catch (err) {
    console.err(err)
    console.log(`Failed to remove the file for ${printerName}`)
  }
}

const deletePrinterWithLogging = async (printerName) => {
  try {
    await deletePrinter(printerName)
  } catch (err) {
    console.err(err)
    console.log(`Failed to delete the printer for ${printerName}`)
  }
}

app.post(
  '/print',
  [
    check('aNumber')
      .matches(/^[aA]\d{8}$/)
      .withMessage('must be a valid A Number'),
    check('pdfData').isString().withMessage('must be a string'),
    check('title').isString().withMessage('must be a string').optional(),
    check('twoSided').isString().withMessage('must be a string').optional(),
    check('landscape').isBoolean().withMessage('must be a boolean').optional(),
    check('colored').isBoolean().withMessage('must be a string').optional(),
  ],
  async (req, res) => {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
      res.status(400).json({ errors: errors.array() })
      return
    }

    const printerName = randomUUID()
    const buffer = Buffer.from(req.body.pdfData, 'base64')
    const filename = '/tmp/' + printerName + '.pdf'

    console.log(
      `Got a new job ${printerName} for A number ${req.body.aNumber}, creating printer...`,
    )
    try {
      await createPrinter(printerName, req.body.aNumber, req.body.colored)
    } catch (err) {
      console.log(`failed to create the printer for ${printerName}`)
      console.error(err)
      res.sendStatus(500)
      return
    }

    console.log(`Created printer for ${printerName}, creating file...`)
    try {
      await fs.writeFile(filename, buffer)
    } catch (err) {
      console.log(`failed to create the file for ${printerName}`)
      console.error(err)
      res.sendStatus(500)
      return
    }

    console.log(`Created the file for ${printerName}, starting print...`)
    try {
      await startPrint(
        printerName,
        req.body.title,
        req.body.twoSided,
        req.body.landscape,
        filename,
      )
    } catch (err) {
      console.log(
        `failed to start the print for ${printerName}, deleting printer and file and exiting`,
      )
      console.error(err)
      res.sendStatus(500)

      await removeFileWithLogging(filename, printerName)
      await deletePrinterWithLogging(printerName)
      return
    }

    console.log(`Started print for ${printerName}, deleting print file...`)
    try {
      await fs.rm(filename)
    } catch (err) {
      console.log(
        `failed to delete print file for ${printerName}, deleting printer and exiting`,
      )
      console.error(err)
      res.sendStatus(500)

      await deletePrinterWithLogging(filename, printerName)
      return
    }

    let finished = false
    while (!finished) {
      console.log(`Waiting for ${printerName} to finish...`)
      try {
        finished = await checkQueue(printerName)
      } catch (err) {
        console.log(
          `failed to check the status of ${printerName}, deleting printer and exiting`,
        )
        console.error(err)
        res.sendStatus(500)

        await deletePrinterWithLogging(filename, printerName)
        return
      }
      if (!finished) {
        await sleep(500)
      }
    }

    console.log(`Print ${printerName} finished, removing printer`)
    try {
      await deletePrinter(printerName)
    } catch (err) {
      console.log(`Failed to delete the printer for ${printerName}`)
      console.error(err)
      res.sendStatus(500)
      return
    }

    console.log(`Finished ${printerName}!`)
    res.send('Submitted File Successfully')
  },
)

app.listen(3000)
console.log('Started on port 3000')
