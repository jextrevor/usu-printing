import { useRef, useState } from 'react'

import './App.css'

const App = () => {
  const fileUpload = useRef()

  const [aNumber, setANumber] = useState('')
  const [title, setTitle] = useState('')
  const [pdfData, setPdfData] = useState('')
  const [fileName, setFileName] = useState('')
  const [colored, setColored] = useState(false)
  const [twoSided, setTwoSided] = useState('')
  const [landscape, setLandscape] = useState(false)
  const [errorMessage, setErrorMessage] = useState('')
  const [successMessage, setSuccessMessage] = useState('')
  const [loading, setLoading] = useState(false)
  const [loadingFile, setLoadingFile] = useState(false)

  return (
    <div className="container">
      <h1>Trevor's USU printing page!</h1>
      <div>
        <input
          type="text"
          onChange={(e) => setANumber(e.target.value)}
          placeholder="A Number"
        />
        <input
          type="text"
          onChange={(e) => setTitle(e.target.value)}
          placeholder="Job Title (optional)"
        />

        <div className="radio">
          <label>
            <input
              type="radio"
              name="color"
              checked={!colored}
              onChange={() => setColored(false)}
            />
            Black and White
          </label>
          <label>
            <input
              type="radio"
              name="color"
              checked={colored}
              onChange={() => setColored(true)}
            />
            Color
          </label>
        </div>
        <div className="radio">
          <label>
            <input
              type="radio"
              name="landscape"
              checked={!landscape}
              onChange={() => setLandscape(false)}
            />
            Portrait
          </label>
          <label>
            <input
              type="radio"
              name="landscape"
              checked={landscape}
              onChange={() => setLandscape(true)}
            />
            Landscape
          </label>
        </div>
        <div className="radio">
          <label>
            <input
              type="radio"
              name="twoSided"
              checked={!twoSided}
              onChange={() => setTwoSided('')}
            />
            One-Sided
          </label>
          <label>
            <input
              type="radio"
              name="twoSided"
              checked={twoSided === 'short'}
              onChange={() => setTwoSided('short')}
            />
            Two-Sided (short edge)
          </label>
          <label>
            <input
              type="radio"
              name="twoSided"
              checked={twoSided === 'long'}
              onChange={() => setTwoSided('long')}
            />
            Two-Sided (long edge)
          </label>
        </div>

        <label>
          {fileName ? fileName + ' ' : ''}
          <button
            className={loadingFile ? 'loading' : ''}
            disabled={loading || loadingFile}
            onClick={() => {
              if (fileUpload.current) {
                fileUpload.current.click()
              }
            }}
          >
            Upload PDF File
          </button>
        </label>
        <button
          className={loading ? 'loading' : ''}
          disabled={loading || !aNumber || !pdfData}
          onClick={async () => {
            setErrorMessage('')
            setSuccessMessage('')
            setLoading(true)
            const result = await fetch('/print', {
              method: 'POST',
              headers: { 'content-type': 'application/json' },
              body: JSON.stringify({
                aNumber,
                title,
                pdfData,
                twoSided,
                landscape,
                colored,
              }),
            })
            setLoading(false)
            if (result.ok) {
              setSuccessMessage(
                `Submitted print job for file ${fileName} and A Number ${aNumber}`,
              )
            } else {
              if (result.status === 400) {
                switch ((await result.json()).errors[0].param) {
                  case 'aNumber':
                    setErrorMessage('Please provide a valid A Number.')
                    break
                  case 'title':
                    setErrorMessage('Error sending title data.')
                    break
                  case 'pdfData':
                    setErrorMessage('Error sending PDF data.')
                    break
                  default:
                    setErrorMessage('Failed to submit print job: Unknown Error')
                }
              } else {
                setErrorMessage(
                  `Failed to submit print job: Internal Server Error`,
                )
              }
            }
          }}
        >
          Send to USU Print Queue
        </button>
        {errorMessage ? (
          <div className="errorMessage">{errorMessage}</div>
        ) : undefined}
        {successMessage ? (
          <div className="successMessage">{successMessage}</div>
        ) : undefined}
      </div>
      <input
        ref={fileUpload}
        type="file"
        tabIndex={-1}
        onChange={() => {
          if (fileUpload.current) {
            setLoadingFile(true)
            setErrorMessage('')
            setSuccessMessage('')
            const fileToRead = fileUpload.current.files[0]
            const reader = new FileReader()
            reader.onload = async (event) => {
              setLoadingFile(false)
              if (
                event.target.result.startsWith('data:application/pdf;base64,')
              ) {
                setPdfData(
                  event.target.result.replace(
                    'data:application/pdf;base64,',
                    '',
                  ),
                )
                setFileName(fileToRead.name)
              } else {
                setErrorMessage('Wrong kind of file. Please upload a PDF file.')
              }
            }
            reader.readAsDataURL(fileToRead)
          }
        }}
      />
    </div>
  )
}

export default App
