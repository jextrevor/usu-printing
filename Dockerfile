FROM node:lts

# Install needed software

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update

RUN ln -fs /usr/share/zoneinfo/America/Denver /etc/localtime
RUN apt-get install -y tzdata
RUN dpkg-reconfigure --frontend noninteractive tzdata

RUN apt-get install -y cups cups-bsd

# Copy code and build

COPY ./client /var/www/client

RUN cd /var/www/client && yarn build

COPY ./server /var/www/server

RUN cd /var/www/server && yarn

COPY ./run.sh /run.sh

CMD ["/run.sh"]
